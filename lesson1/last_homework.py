"""
Задание:
Создайте функцию-генератор чисел Фибоначчи. Примените к ней декоратор, который будет оставлять в
последовательности только чётные числа.
"""

import functools


def fibonacci_decor(function):
    """
    Декоратор для функции Фибоначчи, что оставляет только парные числа в списке
    Детальнее о дектораторах: https://pythonworld.ru/osnovy/dekoratory.html
    :param function:
    :return: list
    """
    def decorated_function(index):
        result_list = list(function(index))  # Получаем итератор и превращаем обьект в список
        filtered_list = []  # Пустой список, который мы будем наполнять парными значениями

        # По результату проверки наполняем список
        for item in result_list:
            if item % 2 == 0:
                filtered_list.append(item)

        return filtered_list

    return decorated_function


@fibonacci_decor  # Детальнее о дектораторах: https://pythonworld.ru/osnovy/dekoratory.html
@functools.lru_cache  # Кэширование: https://pythonworld.ru/moduli/modul-functools.html
def fibonacci(index):
    first, second = 1, 1
    self_index = 0

    while self_index < index:
        yield first
        first, second = second, second + first
        self_index += 1


print(fibonacci(100))
