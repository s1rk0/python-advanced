class PersonBuilder(type):

    @staticmethod
    def __new__(mcs, name, bases, attributes):

        new_attributes = {}

        for key, value in attributes.items():

            new_key = ""
            need_upper = False

            for char in key:
                if char != "_":
                    new_key += char.upper() if need_upper else char
                    need_upper = False
                else:
                    need_upper = True

            new_attributes[new_key] = value

        return type(name, bases, new_attributes)


class Person(metaclass=PersonBuilder):
    def set_name(self, name):
        self.name = name

    def set_last_name(self, last_name):
        self.last_name = last_name

    def set_age(self, age):
        self.age = age

    def get_full_name(self):
        return f"{self.name} {self.last_name}"

    def get_age(self):
        return self.age


if __name__ == '__main__':
    Ivan = Person()
    Ivan.setName("Ivan")
    Ivan.setLastName("Petrov")
    Ivan.setAge(29)

    print(Ivan.getFullName())
    print(Ivan.getAge())
