# Class - Person
# setName
# setLastName
# setAge
# getFullName (name + lastName)
# getAge


def set_name(self, name):
    self.name = name


def set_last_name(self, last_name):
    self.last_name = last_name


def set_age(self, age):
    self.age = age


def get_full_name(self):
    return f"{self.name} {self.last_name}"


def get_age(self):
    return self.age


CLASS_NAME = "Person"
BASES = ()
ATTRIBUTES = {
    "set_name": set_name,
    "set_last_name": set_last_name,
    "set_age": set_age,
    "get_full_name": get_full_name,
    "get_age": get_age,
}


def build_new_class(class_name, base_classes, attributes):
    new_attributes = {}

    for key, value in attributes.items():

        new_key = ""
        need_upper = False

        for char in key:
            if char != "_":
                new_key += char.upper() if need_upper else char
                need_upper = False
            else:
                need_upper = True

        new_attributes[new_key] = value

    return type(class_name, base_classes, new_attributes)


Person = build_new_class(CLASS_NAME, BASES, ATTRIBUTES)

if __name__ == '__main__':
    Ivan = Person()
    Ivan.setName("Ivan")
    Ivan.setLastName("Petrov")
    Ivan.setAge(29)

    print(Ivan.getFullName())
    print(Ivan.getAge())
