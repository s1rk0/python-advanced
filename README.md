## Мои контакты:

* [Telegram](https://t.me/leonidpodriz)
* Skype: live:leonidpodriz
* Email: leonidpodriz@gmail.com

## №1. РАБОТА С СЕТЬЮ В PYTHON: SOCKET И HTTP

Полезные ссылки:

* [Библиотека для упрощения HTTP-запросов](https://habr.com/ru/post/126262/)
* [Requests в Python – Примеры выполнения HTTP запросов](https://python-scripts.com/requests)
* [Python и быстрые HTTP-клиенты](https://habr.com/ru/company/ruvds/blog/472858/) (Для продвинутых)
* [Руководство по работе с HTTP в Python. Библиотека requests](https://khashtamov.com/ru/python-requests/) (Для продвинутых)