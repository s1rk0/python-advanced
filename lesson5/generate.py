import numpy

my_array1 = numpy.arange(0, 40, 2, dtype=numpy.float16)
print(my_array1)

my_array1.resize((2, 5, 2))


# 20 = 4 * 5, 2 * 10
# 20 =  2 * 5 * 2
print(my_array1)
print(my_array1[0][0][0])
print(my_array1[0,0,0])

# 0 20, 100
# (20 - 0) / 100
print()
print(numpy.arange(0, 20, (20 - 0) / 100))
print()
print(numpy.linspace(0, 20, 100))


my_random_array = numpy.random.random_integers(0, 100, 100)

print()
print(my_random_array)

my_random_array2 = numpy.random.rand(100, 100)

print()
print(my_random_array2)
