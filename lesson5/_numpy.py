import numpy
print("Numpy loaded!")

int8 = numpy.int8(129)
print(int8)

int16 = numpy.int16(32768 * 2)
print("==>", int16.itemsize)

uint8 = numpy.uint8(256)
print(uint8.itemsize)

# 8 bit = 1 byte

bool1 = numpy.bool_(256)
bool2 = numpy.bool_(0)
print(bool1)
print(bool2.itemsize)
