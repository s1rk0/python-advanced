import numpy

my_array1 = numpy.array([1.2,2,0,4,5, 2], dtype=numpy.float16)
my_array2 = numpy.array([1.6,2,0,4,5, 2], dtype=numpy.int8)
my_array3 = numpy.array([1.2,2,0,4,5, None], dtype=numpy.bool_)


my_array_test = numpy.array([1,2,0,4,5, 2])
print(my_array_test)
print(my_array_test.dtype)

print(my_array1)
print(my_array2)
print(my_array3)

print(my_array1.ndim)
print(my_array1.shape)
print(my_array1.size)
print(my_array1.itemsize)
print(my_array1.dtype.itemsize)
print(my_array1.dtype)
print(my_array1.data)


my_array_reshaped = my_array1.reshape((3, 2))
print(my_array_reshaped)

my_array1.resize((3,2))
print(my_array1)