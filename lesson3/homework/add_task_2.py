info_data = [("key", "value", 'value2'), ('kay', 'value')]


def data_to_db(data):
    result = []
    for key, *value in data:
        result.append(f"{key}:{','.join(value)}")
    return ";".join(result)


def data_from_db(data):
    result = []

    data_items = data.split(";")
    for items in data_items:
        key, values = items.split(":")
        result.append((key, *values.split(",")))

    return result


data_in_str = data_to_db(info_data)
print(data_in_str)
data_from_str = data_from_db(data_in_str)
print(data_from_str)
