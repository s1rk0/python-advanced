import requests
import json
from bs4 import BeautifulSoup

result = requests.get("https://allo.ua/ua/products/notebooks/proizvoditel-apple/")
soup = BeautifulSoup(result.text, 'lxml')
names_all = soup.find("div", {"class": "category-products"})
names = names_all.find_all("a", {"class": "product-name multiple-lines-crop"})
price = names_all.find_all("span", {"class": "new_sum"})

with open("allo.json", "w", encoding="utf-8") as allo:
    parsed_data = []

    for product_name, product_price in zip(names, price):
        parsed_data.append({
            "name": product_name.text.strip().encode().decode('utf-8'),
            "price": product_price.text.strip().encode().decode('utf-8'),
        })

    json.dump(parsed_data, allo, indent=2, ensure_ascii=False)
