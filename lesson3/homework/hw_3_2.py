import os
import threading
import time

event = threading.Event()
lock = threading.RLock()


def found_file():
    while True:
        with lock:
            try:
                with open("wow.txt", "r") as wow:
                    wow_read = wow.read()
                if "Wow!" in wow_read:
                    print("I found 'Wow!'")
                    event.set()
                    event.clear()
                else:
                    print("'Wow!' not found, Im going to sleep")
            except FileNotFoundError:
                print("File not found, Im going to sleep")
            time.sleep(3)


def delete_file():
    while True:
        event.wait()
        with lock:
            file_path = "./wow.txt"
            os.remove(file_path)
            print("File was deleted")


task1 = threading.Thread(target=found_file)
task2 = threading.Thread(target=delete_file)

task1.start()
task2.start()

task2.join()
task1.join()
