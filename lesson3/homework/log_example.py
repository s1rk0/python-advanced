import logging

logging.basicConfig(
    format='%(levelname)s: %(asctime)s - %(message)s',
    datefmt='%d-%b-%y %H:%M:%S',
    level=logging.DEBUG
)

logging.warning('Admin logged out')
logging.info('Its info')
