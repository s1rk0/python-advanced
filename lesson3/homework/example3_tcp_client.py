import socket

print(" Socket client v1.0 ".center(50, "="))
server_ip = input("Enter server IP [127.0.0.1]:\n>>> ")

if server_ip == "":
    server_ip = '127.0.0.1'

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

sock.connect((server_ip, 8585))

server_message = sock.recv(1024)
print('Server:', server_message.decode())

while True:
    user_input = input("User: ")
    sock.send(user_input.encode('utf-8'))

    server_response = sock.recv(1024)
    print("Server:", server_response.decode('utf-8'))

    if user_input == "close":
        break

sock.close()
