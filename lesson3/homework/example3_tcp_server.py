# example 3 (TCP server socket)
import socket
import threading

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind(('', 8585))
sock.listen(5)


def run_user(client_sock):
    print("New client!")
    client_sock.send("Hello! I can sum your numbers. Ex: 3 60 30 => 93".encode("utf-8"))

    while True:
        user_message = client_sock.recv(1024).decode('utf-8')
        if user_message == 'close':
            client_sock.send("Good bye!".encode("utf-8"))
            client_sock.close()
            break
        else:
            try:
                user_numbers = list(map(int, user_message.strip().split(' ')))
                client_sock.send(str(sum(user_numbers)).encode("utf-8"))
            except ValueError:
                client_sock.send("Invalid data!".encode("utf-8"))


while True:
    try:
        client, addr = sock.accept()
    except KeyboardInterrupt:
        sock.close()
        break
    else:
        task = threading.Thread(target=run_user, args=(client,))
        task.start()
