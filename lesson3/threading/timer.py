import threading
import os


def watcher():
    timer = threading.Timer(5.0, list_files)
    timer.start()


def list_files():
    file_list = os.listdir('.')
    print('\n'.join(file_list))
    watcher()


if __name__ == '__main__':
    watcher()