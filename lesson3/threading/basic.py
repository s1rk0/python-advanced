import threading
import time

task_result = []

target_number = 671088


def payload(started=0, finished=0):
    result = 0
    for number in range(started, finished):
        result += number
    task_result.append(result)


# Part 1
print(" Example 1 ".center(50, "="))
task = threading.Thread(target=payload, args=(0, target_number))

start_time = time.time()
task.start()
task.join()

sum_result = sum(task_result)
delta_time = time.time() - start_time
print(f"Time: {delta_time}\nResult: {sum_result}")

# Part 2
task_result = []
print(" Example 2 ".center(50, "="))

# Создаем потоки
task1 = threading.Thread(target=payload, args=(0, round(target_number / 2)))
task2 = threading.Thread(target=payload, args=(round(target_number / 2), target_number))

# Запускаем потоки
start_time = time.time()
task1.start()
task2.start()

# Говорим интрепритатору подождать выполения этих потоков
task1.join()
task2.join()

sum_result = sum(task_result)
delta_time = time.time() - start_time
print(f"Time: {delta_time}\nResult: {sum_result}")
