import threading
import time

event = threading.Event()


def file_wait():
    while True:
        time.sleep(5)
        print("File found.")
        event.set()
        event.clear()


def file_delete():
    print("Wait for file to delete...")
    while True:
        event.wait()
        print("Delete file.")


task1 = threading.Thread(target=file_delete)
task2 = threading.Thread(target=file_wait)


task1.start()
task2.start()

task2.join()
task1.join()
