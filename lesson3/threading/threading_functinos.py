import threading
import time


def stop_function(second):
    time.sleep(second)


print("Active threading count:\t\t", threading.active_count())

current_thread = threading.current_thread()

print("Current thread:\t\t\t\t", current_thread)
print("Name of current thread:\t\t", current_thread.getName())
print("Is thread alive:\t\t\t", current_thread.is_alive())

# Меняем имя потока 1
current_thread.setName('Python Thread')
print("New name of current thread:\t", current_thread.getName())

# Меняем имя потока 2
current_thread.name = 'Main Python Thread'
print("New name of current thread:\t", current_thread.getName())

task = threading.Thread(target=stop_function, args=(2,))
task.name = "Stop"
task.start()
print("Active threading:\t\t\t", threading.enumerate())
