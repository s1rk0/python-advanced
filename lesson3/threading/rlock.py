import threading

lock = threading.RLock()


def print_something():
    with lock:
        for number in range(100):
            print(number)

        with lock:
            print("Test")


task1 = threading.Thread(target=print_something)
task2 = threading.Thread(target=print_something)

task1.start()
task2.start()

task1.join()
task2.join()
