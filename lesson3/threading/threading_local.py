import threading

data = threading.local()
data.value = 0


def print_value():
    print("Current thread:", threading.current_thread().getName())
    print("Value:", data.value)
    print()


def payload(started=0, finished=0):
    data.value = started

    for number in range(started, finished):
        data.value += number

    print_value()


if __name__ == '__main__':
    task1 = threading.Thread(target=payload, args=(0, 100))
    task2 = threading.Thread(target=payload, args=(0, 1000))

    task1.name = "Task 1"
    task2.name = "Task 2"

    task1.start()
    task2.start()

    task1.join()
    task2.join()

    print_value()
