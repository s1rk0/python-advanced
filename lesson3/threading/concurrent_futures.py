from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
import time

target_number = 82768000


def payload(started=0, finished=0):
    result = 0
    for number in range(started, finished):
        result += number
    return result


def run_executor(executor_class):
    executor = executor_class(max_workers=4)
    start_time = time.time()

    task1 = executor.submit(payload, started=0, finished=int(target_number / 2))
    task2 = executor.submit(payload, started=int(target_number / 2), finished=target_number)

    result = task1.result() + task2.result()
    delta_time = time.time() - start_time

    print(f"Result: {result}\nTime: {delta_time}")


if __name__ == '__main__':
    print("Thread")
    run_executor(ThreadPoolExecutor)
    print()
    print("Process")
    run_executor(ProcessPoolExecutor)