import threading

lock = threading.BoundedSemaphore(value=3)

def print_something():
    with lock:
        for i in range(10):
            print(lock._value, "\t", i)


task1 = threading.Thread(target=print_something)
task2 = threading.Thread(target=print_something)
task3 = threading.Thread(target=print_something)
task4 = threading.Thread(target=print_something)
task5 = threading.Thread(target=print_something)

task1.start()
task2.start()
task3.start()
task4.start()
task5.start()

task1.join()
task2.join()
task3.join()
task4.join()
task5.join()
