import asyncio
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor


def highload_operation(value):
    result = 0
    for i in range(0, value):
        result += i
    return result


async def main(value):
    # выполнение синхронной задачм в pool-е и получение результата.
    with ThreadPoolExecutor() as pool:
        result = await loop.run_in_executor(pool, highload_operation, value)
        print('Result is {}'.format(result))


if __name__ == '__main__':
    print("Sync")
    print('Result is {}'.format(highload_operation(10000000)))
    print('Result is {}'.format(highload_operation(10000001)))

    print("Async")

    loop = asyncio.get_event_loop()

    tasks = asyncio.wait([
        loop.create_task(main(10000000)),
        loop.create_task(main(10000001)),
    ])

    loop.run_until_complete(tasks)
    loop.close()
