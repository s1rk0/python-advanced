def concat_sequence(*args):
    for sequence in args:
        yield from sequence


s1 = range(1, 4)
s2 = range(5, 7)
s3 = range(40, 45)
s4 = range(50, 71)
s5 = range(74, 78)

gen = concat_sequence(s1, s2, s3, s4, s5)

print(list(gen))
