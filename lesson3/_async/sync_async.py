import asyncio
import time


def sync_function():
    print("Sync function is stated.")
    time.sleep(3)
    print("Sync function is completed!")


sync_function()
sync_function()

print()


@asyncio.coroutine
def async_function():
    print("Async function is stated.")
    yield from asyncio.sleep(3)
    print("Async function is completed!")


diary = asyncio.get_event_loop()

diary_page = [
    diary.create_task(async_function()),
    diary.create_task(async_function()),
]

tasks = asyncio.wait(diary_page)

diary.run_until_complete(tasks)
diary.close()
