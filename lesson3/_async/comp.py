# Обяснить time.sleep()

import asyncio


async def async_sum(*numbers):
    print("Sum started")
    await asyncio.sleep(0)
    print(sum(numbers))
    await asyncio.sleep(0)
    print("Sum finished")


event_loop = asyncio.get_event_loop()

task_list = [
    event_loop.create_task(async_sum(number1, number2))
    for number1, number2 in zip(range(5), range(5, 10))
]

tasks = asyncio.wait(task_list)
event_loop.run_until_complete(tasks)
event_loop.close()
