def coroutine(function):
    def wrapper(*args, **kwargs):
        generator = function(*args, **kwargs)
        generator.send(None)
        return generator

    return wrapper


@coroutine
def is_divider(number):
    print("Coroutine is started!")

    while True:
        value = yield

        if number % value == 0:
            print(value)


my_cor1 = is_divider(100)
my_cor2 = is_divider(100)
my_cor3 = is_divider(100)
my_cor4 = is_divider(100)
my_cor5 = is_divider(100)