from xml.etree import ElementTree

root = ElementTree.Element("database")

for number in range(10):
    sub_element = ElementTree.SubElement(root, 'number')
    sub_element.text = str(number)

tree = ElementTree.ElementTree(root)
tree.write('data/numbers.xml')
