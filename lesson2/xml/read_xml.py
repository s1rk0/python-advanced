from xml.etree import ElementTree

tree = ElementTree.parse('data/db.xml')
root = tree.getroot()

for children in root:
    # children.tag - Tag name
    # children.attrib - Data-attribs of tag (dict)
    
    print(children.tag, children.attrib['id'])

    for student_data in children:
        print(student_data.tag, ':', student_data.text)

    print()
