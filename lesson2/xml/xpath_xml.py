from xml.etree import ElementTree

tree = ElementTree.parse('data/db.xml')
data = tree.getroot()

for student in data:
    print(student.tag, student.attrib['id'])

    print("{name} : {group} : {mark}".format(
        name=student.find('./name').text,
        group=student.find('./group').text,
        mark=student.find('./mark').text,
    ))

    print()

print("Names in DB:")
for index, name in enumerate(data.findall('./student[@id]/name'), 1):
    print(index, name.text)
