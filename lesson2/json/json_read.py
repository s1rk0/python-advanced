import json

print(" Example 1 ".center(50, '='))

with open('users.json') as json_file:
    data = json_file.read()

json_data = json.loads(data)
print(json_data)

print(" Example 2 ".center(50, '='))

with open('users.json') as json_file:
    json_data = json.load(json_file)
    print(json_data['users'][0]['name'])
