import datetime
import json


class DateEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.date):
            return {
                "value": obj.strftime("%Y/%m/%d"),
                "__data__": True,
            }

        return super().default(self, obj)


data = {
    'programmers': [
        {
            "name": "Leonid",
            "lang": ['Python', "JS", "PHP"],
            "birthday": datetime.date(2000, 2, 3)
        },
    ]
}

with open('prog_hooks.json', "w") as json_file:
    json.dump(data, json_file, indent=2, cls=DateEncoder)
