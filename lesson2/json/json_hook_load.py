import json
import datetime


def convert_to_datetime(dictionary):
    if '__data__' in dictionary:
        return datetime.datetime.strptime(dictionary['value'], "%Y/%m/%d").date()
    return dictionary


with open('prog_hooks.json', 'r') as json_file:
    data = json.load(json_file, object_hook=convert_to_datetime)
    print(data)
