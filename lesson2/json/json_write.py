import json

data = {
    'programmers': [
        {
            "name": "Leonid",
            "lang": ['Python', "JS", "PHP"],
        },
    ]
}

with open('prog.json', "w") as json_file:
    json.dump(data, json_file, indent=2)
