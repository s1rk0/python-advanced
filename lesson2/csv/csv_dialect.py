import csv

# Old version:

# class MyDialect(csv.Dialect):
#     quoting = csv.QUOTE_ALL
#     quotechar = "\'"
#     delimiter = "|"
#     lineterminator = '\n'

# csv.register_dialect('MyDialect', MyDialect)

csv.register_dialect('MyDialect', quoting=csv.QUOTE_ALL, quotechar="\'", delimiter=".", lineterminator='\n')

with open('data/dialect.csv', 'w') as csv_file:
    writer = csv.writer(csv_file, dialect='MyDialect')
    writer.writerow([1, 'dialect', 4, 'dialect'])
    writer.writerow([1, 'dialect', 4, 'dialect'])
    writer.writerow([1, 'dialect', 4, 'dialect'])
    writer.writerow([1, 'dialect', 4, 'dialect'])
    writer.writerow([1, 'dialect', 4, 'dialect'])
    writer.writerow([1, 'dialect', 4, 'dialect'])
