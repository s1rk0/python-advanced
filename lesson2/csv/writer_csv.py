import csv

with open('data/test.csv', 'w') as csv_file:
    writer = csv.writer(csv_file, quoting=csv.QUOTE_MINIMAL)
    writer.writerow([1, 'test', 4, 'test'])
    writer.writerow([1, 'test', 4, 'test'])
    writer.writerow([1, 'test', 4, 'test'])
    writer.writerow([1, 'test', 4, 'test'])
    writer.writerow([1, 'test', 4, 'test'])
    writer.writerow([1, 'test', "4", 'te,st'])
