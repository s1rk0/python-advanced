import csv

FILE_NAME = './data/products.csv'

print(" CSV EXAMPLES ".center(50, "="))

with open(FILE_NAME, 'r') as csv_file:
    reader = csv.reader(csv_file)

    print("Lines start:", reader.line_num)
    print("Dialect delimiter:", reader.dialect.delimiter)
    print()

    for line in reader:
        print(line)

    print()
    print("Lines end:", reader.line_num)
