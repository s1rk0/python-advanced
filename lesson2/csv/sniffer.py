import csv

sniffer = csv.Sniffer()

with open('data/dialect.csv', 'r') as csv_file:
    file_content = csv_file.read()
    dialect = sniffer.sniff(file_content)

print('Delimiter:', dialect.delimiter)
print('Quoting char:', dialect.quotechar)

print()

with open('data/dialect.csv', 'r') as csv_file:
    reader = csv.reader(csv_file)

    for line in reader:
        print(line)

print()

with open('data/dialect.csv', 'r') as csv_file:
    reader = csv.reader(csv_file, dialect=dialect)

    for line in reader:
        print(line)
