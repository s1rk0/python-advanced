import csv

with open('data/test_dict.csv', 'w') as csv_file:
    writer = csv.DictWriter(csv_file, fieldnames=('Student', 'Group', 'Mark', 'Status'))
    writer.writeheader()
    writer.writerow({
        'Student': 'Leonid Podriz',
        'Group': 'Python Django',
        'Mark': 5,
        'Status': 'finished',
    })

    writer.writerow({
        'Student': 'Ivan Petrov',
        'Group': 'Python Advanced',
        'Mark': 7,
        'Status': 'in progress',
    })
