import math
import sqlite3

connection = sqlite3.connect(':memory:')

connection.execute("CREATE TABLE 'products' (id, name, price, type);")
connection.execute("INSERT INTO 'products' (id, name, price, type) VALUES (1, 'products1', 1, 'some_type')")
connection.execute("INSERT INTO 'products' (id, name, price, type) VALUES (2, 'products2', 2, 'some_type')")
connection.execute("INSERT INTO 'products' (id, name, price, type) VALUES (3, 'products3', 3, 'some_type')")
connection.execute("INSERT INTO 'products' (id, name, price, type) VALUES (4, 'products4', 4, 'some_type')")
connection.execute("INSERT INTO 'products' (id, name, price, type) VALUES (5, 'products5', 5, 'some_type')")
connection.execute("INSERT INTO 'products' (id, name, price, type) VALUES (6, 'products6', 6, 'some_type')")

connection.commit()

for name, price in connection.execute("SELECT name, price FROM 'products'").fetchall():
    print(f"{name} is {price}$")


class Aggregate:

    def __init__(self):
        self.count_price = 0
        self.count = 0

    def step(self, value):
        self.count_price += value
        self.count += 1

    def finalize(self):
        return math.ceil(self.count_price / self.count)


def concatenate(row):
    row_in_str = list(map(str, row))
    return "|".join(row_in_str)


connection.create_function('concatenate', 2, concatenate)
connection.create_aggregate('sum_price', 1, Aggregate)

cursor = connection.cursor()
cursor.execute("SELECT sum_price(price) AS count_price FROM products ")
total_price = cursor.fetchone()[0]
print(f"Total price: {total_price}$")

cursor.execute("SELECT concatenate(name, type) FROM 'products'; ")
result = cursor.fetchall()
print(result)

connection.close()
