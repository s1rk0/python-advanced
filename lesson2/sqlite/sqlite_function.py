import sqlite3

connection = sqlite3.connect(':memory:')

connection.execute("CREATE TABLE 'products' (id, name, price, type);")
connection.execute("INSERT INTO 'products' (id, name, price, type) VALUES (1, 'products1', 1, 'some_type')")
connection.execute("INSERT INTO 'products' (id, name, price, type) VALUES (2, 'products2', 2, 'some_type')")
connection.execute("INSERT INTO 'products' (id, name, price, type) VALUES (3, 'products3', 3, 'some_type')")
connection.execute("INSERT INTO 'products' (id, name, price, type) VALUES (4, 'products4', 4, 'some_type')")
connection.execute("INSERT INTO 'products' (id, name, price, type) VALUES (5, 'products5', 5, 'some_type')")
connection.execute("INSERT INTO 'products' (id, name, price, type) VALUES (6, 'products6', 6, 'some_type')")

connection.commit()


def concatenate(*args):
    row_in_str = list(map(str, args))
    row = " | ".join(row_in_str)
    new_line = "=" * len(row)
    return row + '\n' + new_line


connection.create_function('concatenate', 4, concatenate)

cursor = connection.cursor()

cursor.execute("SELECT concatenate(id, name, price, type) FROM 'products'; ")
result = cursor.fetchall()
for item in result:
    print(item[0])

connection.close()
