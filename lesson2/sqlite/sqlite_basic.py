import sqlite3

connection = sqlite3.connect(':memory:')

connection.execute("CREATE TABLE 'products' (id, name, price, type);")
connection.execute("INSERT INTO 'products' (id, name, price, type) VALUES (1, 'Milk', 2, 'drink')")
connection.execute("INSERT INTO 'products' (id, name, price, type) VALUES (2, 'Sweet', 0.1, 'drink')")
connection.commit()

data_from_db = connection.execute("SELECT * FROM 'products'")
print(data_from_db.fetchall())

for name, price in connection.execute("SELECT name, price FROM 'products'").fetchall():
    print(f"{name} is {price}$")

connection.close()
