import csv
from xml.etree import ElementTree

with open('homework_data/user.csv', 'r') as csv_file:
    reader = csv.DictReader(csv_file)
    data = list(reader)

root = ElementTree.Element('data')

for person in data:
    person_element = ElementTree.SubElement(root, 'person')

    for key, value in person.items():
        info_element = ElementTree.SubElement(person_element, str(key))
        info_element.text = str(value)

tree = ElementTree.ElementTree(root)
tree.write('homework_data/user.xml')
